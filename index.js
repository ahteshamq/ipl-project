const { match } = require("assert");
const fs = require("fs");
const papa = require("papaparse");
const path = require("path");

// Match data
const matchFilePath = path.join(__dirname, "/src/data/matches.csv");
try {
  matchesFile = fs.readFileSync(matchFilePath);
  matches = papa.parse(matchesFile.toString(), { header: true }).data;
} catch (error) {
  console.error("Error either while reading or parsing: ", error);
}

// Delivery data
const deliveryFilePath = path.join(__dirname, "/src/data/deliveries.csv");
try {
  const deliveriesFile = fs.readFileSync(deliveryFilePath);
  deliveries = papa.parse(deliveriesFile.toString(), {
    header: true,
  }).data;
} catch (error) {
  console.error("Error either while reading or parsing: ", error);
}

// function modules

const pathProblem1 = path.join(__dirname, "/src/server/1-matches-per-year.cjs");
const seasonMatches = require(pathProblem1);

const pathProblem2 = path.join(
  __dirname,
  "/src/server/2-matches-won-per-team-per-year.cjs"
);
const seasonMatchesTeams = require(pathProblem2);

const pathProblem3 = path.join(
  __dirname,
  "/src/server/3-Extra-runs-conceded-per-team-in-the-year-2016.cjs"
);
const extraRun2016 = require(pathProblem3);

const pathProblem4 = path.join(
  __dirname,
  "/src/server/4-Top-10-economical-bowlers-in-the-year-2015.cjs"
);
const avgTop10Bowler = require(pathProblem4);

const pathProblem5 = path.join(
  __dirname,
  "/src/server/5-Find-the-number-of-times-each-team-won-the-toss-and-also-won-the-match.cjs"
);
const winnerTossMatch = require(pathProblem5);

const pathProblem6 = path.join(
  __dirname,
  "/src/server/6-Find-a-player-who-has-won-the-highest-number-of-Player-of-the-Match-awards-for-each-season.cjs"
);
const mostMOMSeason = require(pathProblem6);

const pathProblem7 = path.join(
  __dirname,
  "/src/server/7-Find-the-strike-rate-of-a-batsman-for-each-season.cjs"
);
const strikeRateSeason = require(pathProblem7);

const pathProblem8 = path.join(
  __dirname,
  "/src/server/8-Find-the-highest-number-of-times-one-player-has-been-dismissed-by-another-player.cjs"
);
const playerDismissalPlayer = require(pathProblem8);

const pathProblem9 = path.join(
  __dirname,
  "/src/server/9-Find-the-bowler-with-the-best-economy-in-super-overs.cjs"
);
const bestSuperEco = require(pathProblem9);

//Json files path

const seasonMatchesJson = path.join(
  __dirname,
  "src/public/output/1-matches-per-year.json"
);
const seasonMatchesWonTeamsJson = path.join(
  __dirname,
  "src/public/output/2-matches-won-per-team-per-year.json"
);
const extraRuns2016Json = path.join(
  __dirname,
  "/src/public/output/3-Extra-runs-conceded-per-team-in-the-year-2016.json"
);
const top10EcoBowlJson = path.join(
  __dirname,
  "/src/public/output/4-Top-10-economical-bowlers-in-the-year-2015.json"
);
const wonTossMatchJson = path.join(
  __dirname,
  "/src/public/output/5-Find-the-number-of-times-each-team-won-the-toss-and-also-won-the-match.json"
);
const momMostSeasonJson = path.join(
  __dirname,
  "/src/public/output/6-Find-a-player-who-has-won-the-highest-number-of-Player-of-the-Match-awards-for-each-season.json"
);
const srSeasonWiseJson = path.join(
  __dirname,
  "/src/public/output/7-Find-the-strike-rate-of-a-batsman-for-each-season.json"
);
const playerDismissalPlayerJson = path.join(
  __dirname,
  "/src/public/output/8-Find-the-highest-number-of-times-one-player-has-been-dismissed-by-another-player.json"
);
const superOverBowlerJson = path.join(
  __dirname,
  "/src/public/output/9-Find-the-bowler-with-the-best-economy-in-super-overs.json"
);

//Problem 1
const resSeasonMatches = seasonMatches(matches);
console.log(resSeasonMatches);
fs.writeFile(seasonMatchesJson, JSON.stringify(resSeasonMatches), (err) => {
  if (err) console.log(err);
  else {
    console.log("File written successfully\n");
  }
});

/*
// Problem 2
const resSeasonMatchesTeams = seasonMatchesTeams(matches);
console.log(resSeasonMatchesTeams);
fs.writeFile(
  seasonMatchesWonTeamsJson,
  JSON.stringify(resSeasonMatchesTeams),
  (err) => {
    if (err) console.log(err);
    else {
      console.log("File written successfully\n");
    }
  }
);
*/
/*
//Problem 3
const resExtra2016 = extraRun2016(matches, deliveries);
console.log(resExtra2016);
fs.writeFile(extraRuns2016Json, JSON.stringify(resExtra2016), (err) => {
  if (err) console.log(err);
  else {
    console.log("File written successfully\n");
  }
});
*/

/*
//Problem 4
const resAvgTop10Bowler = avgTop10Bowler(matches, deliveries);
console.log(resAvgTop10Bowler);
fs.writeFile(top10EcoBowlJson, JSON.stringify(resAvgTop10Bowler), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Files written succefully.\n");
  }
});
*/

/*
// Problem 5
const resWonTossAndMatch = winnerTossMatch(matches);
//console.log(resWonTossAndMatch);
fs.writeFile(wonTossMatchJson, JSON.stringify(resWonTossAndMatch), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("File written successfully.");
  }
});
*/
/*
//Problem 6
const resMostMOMSeason = mostMOMSeason(matches);
// console.log(resMostMOMSeason)
fs.writeFile(momMostSeasonJson, JSON.stringify(resMostMOMSeason), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("File written successfully.");
  }
});
*/

/*
//Problem 7
const resStrikeRateSeason = strikeRateSeason(matches, deliveries);
//console.log(resStrikeRateSeason)
fs.writeFile(srSeasonWiseJson, JSON.stringify(resStrikeRateSeason), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("File written successfully.");
  }
});
*/

/*
// Problem 8
const res8 = playerDismissalPlayer(deliveries);
console.log(res8);
fs.writeFile(playerDismissalPlayerJson, JSON.stringify(res8), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("File written successfully.");
  }
});
*/

/*
// Problem 9
const res9 = bestSuperEco(deliveries);
console.log(res9);
fs.writeFile(superOverBowlerJson, JSON.stringify(res9), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("File written successfully.");
  }
});
*/
