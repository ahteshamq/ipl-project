function playerDismissedByPlayer(arrayDel) {
  if (arrayDel === undefined || !Array.isArray(arrayDel)) {
    return {};
  }

  let playerDismissal = arrayDel.reduce((accumulator, currentValue) => {
    if (
      currentValue.dismissal_kind !== undefined &&
      currentValue.player_dismissed !== ""
    ) {
      if (accumulator.hasOwnProperty(currentValue.batsman)) {
        if (
          accumulator[currentValue.batsman].hasOwnProperty(currentValue.bowler)
        ) {
          accumulator[currentValue.batsman][currentValue.bowler] += 1;
        } else {
          accumulator[currentValue.batsman][currentValue.bowler] = 1;
        }
      } else {
        //let bowlerCur = currentValue.bowler;
        accumulator[currentValue.batsman] = {};
        accumulator[currentValue.batsman] = {
          [currentValue.bowler]: 1,
        };
      }
    }
    return accumulator;
  }, {});

  let res = Object.entries(playerDismissal).map((players) => {
    // Player[1] array of bowler and no of times they dismissed the batsman
    let sortedBowler = Object.entries(players[1]).sort((bowler1, bowler2) => {
      // [1] = ['Bowler Name', no of times he dismissed the batsman]
      let wicket1 = bowler1[1];
      let wicket2 = bowler2[1];
      return wicket2 - wicket1;
    });

    // [0] contains first element of soertedBowler array, that is, bowler with most wickets
    // [0][1] no of times he dismissed the batsman
    let mostWickets = sortedBowler[0][1];

    // All bowler/bowlers who dismissed the batsman most.
    let mostWicketTakers = sortedBowler.filter((data) => {
      return data[1] == mostWickets;
    });

    //[0] batsman name.
    return { [players[0]]: Object.fromEntries(mostWicketTakers) };
  });

  return res;
}

module.exports = playerDismissedByPlayer;
