function strikeRateEachSeason(arrayMatch, arrayDelivery) {
  if (
    arrayMatch === undefined ||
    !Array.isArray(arrayMatch) ||
    arrayDelivery === undefined ||
    !Array.isArray(arrayDelivery)
  ) {
    return {};
  }

  let idSeason = arrayMatch.reduce((accumulator, currentvalue) => {
    if (accumulator.hasOwnProperty(currentvalue.id)) {
    } else {
      accumulator[currentvalue.id] = currentvalue.season;
    }
    return accumulator;
  }, {});

  let batsamanData = arrayDelivery.reduce((accumulator, currentValue) => {
    let season = idSeason[currentValue.match_id];
    let run = Number(currentValue.batsman_runs);
    if (accumulator.hasOwnProperty(currentValue.batsman)) {
      if (accumulator[currentValue.batsman].hasOwnProperty(season)) {
        accumulator[currentValue.batsman][season]["run"] += run;
        accumulator[currentValue.batsman][season]["ball"] += 1;
      } else {
        accumulator[currentValue.batsman][season] = {
          ball: 1,
          run: run,
        };
      }
    } else {
      accumulator[currentValue.batsman] = {};
      accumulator[currentValue.batsman][season] = {
        ball: 1,
        run: run,
      };
    }
    return accumulator;
  }, {});

  let res = Object.entries(batsamanData).map((player) => {
    //[1] contains players's season wise data
    let stat = Object.entries(player[1]).map((season) => {
      //[1] contains seasons's ball played and run scored by batsman
      let runRate = (season[1].run / season[1].ball) * 100;
      //[0] contains season
      return [season[0], runRate.toFixed(2)];
    });
    objStat = Object.fromEntries(stat);
    //[0] contains player name
    return [player[0], objStat];
  });

  return res;
}

module.exports = strikeRateEachSeason;
