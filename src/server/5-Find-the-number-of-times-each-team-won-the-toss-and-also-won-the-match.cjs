function wonTossAndMatch(arrMatch) {
  if (arrMatch === undefined || !Array.isArray(arrMatch)) {
    return {};
  }
  let res = arrMatch.reduce((accumulator, currentValue) => {
    if (currentValue.toss_winner === currentValue.winner) {
      let team = currentValue.toss_winner;
      if (team in accumulator) {
        accumulator[team] = accumulator[team] + 1;
      } else {
        accumulator[team] = 1;
      }
    }
    return accumulator;
  }, {});
  return res;
}

module.exports = wonTossAndMatch;
