function highestMOM(arrayMatches) {
  if (arrayMatches === undefined || !Array.isArray(arrayMatches)) {
    return {};
  }

  let seasonMOM = arrayMatches.reduce((accumulator, currentValue) => {
    if (currentValue.season in accumulator) {
      if (currentValue.player_of_match in accumulator[currentValue.season]) {
        accumulator[currentValue.season][currentValue.player_of_match] += 1;
      } else {
        accumulator[currentValue.season][currentValue.player_of_match] = 1;
      }
    } else {
      accumulator[currentValue.season] = {};
      accumulator[currentValue.season][currentValue.player_of_match] = 1;
    }

    return accumulator;
  }, {});

  let res = Object.entries(seasonMOM).map((season) => {
    //[1] contains players data [name, no of times they have won MOM award]
    let TopPlayer = Object.entries(season[1]).sort((data1, data2) => {
      // [1] contains no of times the player has won MOM award
      let mom1 = data1[1];
      let mom2 = data2[1];
      return mom2 - mom1;
    });
    // season[0] contains season name, and TopPlayer[0] contains player with most MOM award[name, awards]
    return [season[0], TopPlayer[0]];
  });

  return Object.fromEntries(res);
}

module.exports = highestMOM;
