function seasons(array) {
  if (array === undefined || !Array.isArray(array)) {
    return {};
  }
  let res = array.reduce((accumulator, currentValue) => {
    if (accumulator.hasOwnProperty(currentValue.season)) {
      accumulator[currentValue.season] += 1;
    } else {
      accumulator[currentValue.season] = 1;
    }
    return accumulator;
  }, {});
  return res;
}

module.exports = seasons;
