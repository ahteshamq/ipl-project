function extraRun2016(arrayMatch, arrayDel) {
  if (
    arrayMatch === undefined ||
    !Array.isArray(arrayMatch) ||
    arrayDel === undefined ||
    !Array.isArray(arrayDel)
  ) {
    return {};
  }
  let arr16 = arrayMatch
    .filter((match) => {
      return match.season === "2016";
    })
    .map((match) => {
      return match.id;
    });

  let res = arrayDel.reduce((accumulator, currentValue) => {
    if (arr16.includes(currentValue.match_id)) {
      if (accumulator.hasOwnProperty(currentValue.bowling_team)) {
        accumulator[currentValue.bowling_team] += Number(
          currentValue.extra_runs
        );
      } else {
        accumulator[currentValue.bowling_team] = Number(
          currentValue.extra_runs
        );
      }
    }
    return accumulator;
  }, {});
  return res;
}

module.exports = extraRun2016;
