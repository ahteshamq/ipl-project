function bestSuperEco(arrDel) {
  if (arrDel === undefined || !Array.isArray(arrDel)) {
    return {};
  }
  let runBallPlayer = arrDel.reduce((accumulator, currentValue) => {
    if (currentValue.is_super_over === "1") {
      if (accumulator.hasOwnProperty(currentValue.bowler)) {
        accumulator[currentValue.bowler]["ball"] += 1;
        let run =
          Number(currentValue.total_runs) -
          Number(currentValue.bye_runs) -
          Number(currentValue.legbye_runs);
        accumulator[currentValue.bowler]["run"] += run;
      } else {
        let run =
          Number(currentValue.total_runs) -
          Number(currentValue.bye_runs) -
          Number(currentValue.legbye_runs);
        accumulator[currentValue.bowler] = {
          ball: 1,
          run: run,
        };
      }
    }
    return accumulator;
  }, {});

  let res = Object.entries(runBallPlayer)
    .map((bowler) => {
      //[1] contains object with ball and run properties of bowler.
      let ball = bowler[1].ball;
      let run = bowler[1].run;
      let economy = (run * 6) / ball;
      // [0] contains bowler name.
      return [bowler[0], economy];
    })
    .sort((playerA, playerB) => {
      //[1] contains economy of bowler.
      let eco1 = playerA[1];
      let eco2 = playerB[1];
      return eco1 - eco2;
    });
  //[0] Best bowler's data as array['bowler name', economy]
  return res[0];
}

module.exports = bestSuperEco;
