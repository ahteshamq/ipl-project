function seasonTeamsWinsMatches(array) {
  if (array === undefined || !Array.isArray(array)) {
    return {};
  }
  let team = array.reduce((accumulator, currentValue) => {
    if (currentValue.result === "normal") {
      if (accumulator.hasOwnProperty(currentValue.winner)) {
        if (
          accumulator[currentValue.winner].hasOwnProperty([currentValue.season])
        ) {
          accumulator[currentValue.winner][currentValue.season] += 1;
        } else {
          accumulator[currentValue.winner][currentValue.season] = 1;
        }
      } else {
        accumulator[currentValue.winner] = {};
        accumulator[currentValue.winner][currentValue.season] = 1;
      }
    }
    return accumulator;
  }, {});
  return team;
}
module.exports = seasonTeamsWinsMatches;

//currentValue.result === "normal";
