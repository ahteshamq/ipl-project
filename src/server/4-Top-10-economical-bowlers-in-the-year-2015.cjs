function top10EcoBowler2015(arrMatch, arrDel) {
  if (
    arrMatch === undefined ||
    !Array.isArray(arrMatch) ||
    arrDel === undefined ||
    !Array.isArray(arrDel)
  ) {
    return {};
  }
  let arr15 = arrMatch
    .filter((match) => {
      return match.season === "2015";
    })
    .map((match) => {
      return match.id;
    });

  let arrRunBallPlayer = arrDel.reduce((accumulator, currentValue) => {
    if (arr15.includes(currentValue.match_id)) {
      if (accumulator.hasOwnProperty(currentValue.bowler)) {
        accumulator[currentValue.bowler]["ball"] += 1;
        accumulator[currentValue.bowler]["run"] +=
          Number(currentValue.total_runs) -
          Number(currentValue.bye_runs) -
          Number(currentValue.legbye_runs);
      } else {
        accumulator[currentValue.bowler] = {};
        let run =
          Number(currentValue.total_runs) -
          Number(currentValue.bye_runs) -
          Number(currentValue.legbye_runs);
        accumulator[currentValue.bowler] = {
          ball: 1,
          run: run,
        };
      }
    }
    return accumulator;
  }, {});

  let res = Object.entries(arrRunBallPlayer)
    .map((player) => {
      // [1] contains bowler data object with properties ball and run
      let economy = (player[1].run * 6) / player[1].ball;
      //[0] contains bowler name
      return [player[0], economy.toFixed(2)];
    })
    .sort((data1, data2) => {
      //eco: economy
      // [1] contains economy of the bowler
      const eco1 = data1[1];
      const eco2 = data2[1];
      return eco1 - eco2;
    });
  return Object.fromEntries(res.slice(0, 10));
}

module.exports = top10EcoBowler2015;
